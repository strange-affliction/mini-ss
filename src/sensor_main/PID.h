//   ____ ___  ____  ____
//  / __// _ \/ __ \/ __/
// / _ \/ // / /_/ / _/
// \___/____/\____/_/
//
// Mini SS Workshop
// Whitespace Week 2019
//
// This file is adapted from PIDLoop.h from the Pixy Arduino Library

#define MIN_SERVO_MILLI_DEG           0
#define MAX_SERVO_MILLI_DEG           180000L
#define CENTER_SERVO_MILLI_DEG        ((MAX_SERVO_MILLI_DEG-MIN_SERVO_MILLI_DEG)/2)
#define PID_MAX_INTEGRAL              2000

class PID
{
public:
    PID(int32_t pgain, int32_t igain, int32_t dgain)
    {
        m_pgain = pgain;
        m_igain = igain;
        m_dgain = dgain;

        reset();
    }

    void reset()
    {
        m_command = CENTER_SERVO_MILLI_DEG;
        m_integral = 0;
        m_prevError = 0x80000000L;
    }

    void update(int32_t error)
    {
        int32_t pid;

        if (m_prevError!=0x80000000L)
        {
            // integrate integral
            m_integral += error;
            // bound the integral
            if (m_integral>PID_MAX_INTEGRAL)
              m_integral = PID_MAX_INTEGRAL;
            else if (m_integral<-PID_MAX_INTEGRAL)
              m_integral = -PID_MAX_INTEGRAL;

            // calculate PID term
            pid = (error*m_pgain + ((m_integral*m_igain)>>4) + (error - m_prevError)*m_dgain)>>10;

            m_command += pid; // since servo is a position device, we integrate the pid term
            if (m_command>MAX_SERVO_MILLI_DEG)
            m_command = MAX_SERVO_MILLI_DEG;
            else if (m_command<MIN_SERVO_MILLI_DEG)
            m_command = MIN_SERVO_MILLI_DEG;
        }

        // retain the previous error val so we can calc the derivative
        m_prevError = error;
    }

    int32_t m_command;

private:
    int32_t m_pgain;
    int32_t m_igain;
    int32_t m_dgain;

    int32_t m_prevError;
    int32_t m_integral;
};
