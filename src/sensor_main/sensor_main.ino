//   ____ ___  ____  ____
//  / __// _ \/ __ \/ __/
// / _ \/ // / /_/ / _/
// \___/____/\____/_/
//
// Mini SS Workshop
// Whitespace Week 2019

// Debug flag
//#define DEBUG

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Include Libraries                                                                     *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#include <Servo.h>
#include <Pixy2I2C.h>
#include <stdio.h>
#include <math.h>
#include "PID.h"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Global Position Configuration                                                         *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define SENSOR_G_X        0 // in mm
#define SENSOR_G_Y        0 // in mm

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Target Identification Configuration                                                   *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define AIM_RADIUS        1        // in pixels
#define TARGET_WIDTH      100      // in mm
#define PIXY_FOCAL_LEN    273.664

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Hardware Pin Configuration                                                            *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define PAN_SERVO         10      // use a PWM pin
#define TILT_SERVO        9       // use a PWM pin

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Communication Parameters                                                              *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define START_CHAR        0xF5    // start char for XBee comms

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Global Variables                                                                      *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Pixy2I2C pixy;
PID panLoop(50000, 0, 20000);     // PID loop for pan. (P_gain, I_gain, D_gain)
PID tiltLoop(50000, 0, 20000);    // PID legaloop for tilt. (P_gain, I_gain, D_gain)

Servo panServo;
Servo tiltServo;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Forward Declarations for Subroutines                                                  *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void setServos(int32_t const& pan, int32_t const& tilt);
int32_t computeRange(int32_t const& target_width);
void getOwnGlobalPosition(int32_t& own_x, int32_t& own_y);
void computeGlobalCoordinates(
  int32_t const& own_x,
  int32_t const& own_y,
  int32_t const& range,
  int32_t const& azimuth,
  int32_t& g_x,
  int32_t& g_y);
void sendTarget(int32_t const& g_x, int32_t const& g_y);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Arduino Setup - This runs once only                                                   *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void setup()
{
    pixy.init();
    panServo.attach(PAN_SERVO);
    tiltServo.attach(TILT_SERVO);
    Serial.begin(9600);
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Arduino Loop - This loops forever                                                     *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void loop()
{
    int32_t panOffset, tiltOffset;

    // Get target bounding box from Pixy
    pixy.ccc.getBlocks();

    // If there are detect blocks
    if (pixy.ccc.numBlocks)
    {
        // Get offset of block
        Block& target = pixy.ccc.blocks[0];
        panOffset = (int32_t)pixy.frameWidth/2 - (int32_t)target.m_x;
        tiltOffset = (int32_t)target.m_y - (int32_t)pixy.frameHeight/2;

        // If target center is close to center of frame
        if (panOffset <= AIM_RADIUS && tiltOffset <= AIM_RADIUS)
        {
            // Get target range using passive ranging
            int32_t range = computeRange((int32_t)target.m_width);

            // Get target azimuth
            int32_t azimuth = panLoop.m_command - CENTER_SERVO_MILLI_DEG; // millidegrees from center

            // Get own global position
            int32_t own_x, own_y;
            getOwnGlobalPosition(own_x, own_y);

            // Compute target global coordinates
            int32_t g_x, g_y;
            computeGlobalCoordinates(own_x, own_y, range, azimuth, g_x, g_y);

            #ifdef DEBUG
            Serial.print("r:");
            Serial.print(range);
            Serial.print(" a:");
            Serial.print(azimuth/1000);
            Serial.print(" x:");
            Serial.print(g_x);
            Serial.print(" y:");
            Serial.print(g_y);
            Serial.println();
            #else
            // Send target to shooter
            sendTarget(g_x, g_y);
            #endif
        }

        // update loops
        panLoop.update(panOffset);
        tiltLoop.update(tiltOffset);

        // set pan and tilt servos
        setServos(panLoop.m_command, tiltLoop.m_command);
    }
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Function Definitions - These are the processing logics behind each line in loop()     *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

/**
 * Set the gimbal to desired pan and tilt.
 *
 * @param pan   Pan angle in millidegrees.
 * @param tilt  Tilt angle in millidegrees.
 */
void setServos(int32_t const& pan, int32_t const& tilt)
{
    // TO FILL IN (copy Launcher code)
}

/**
 * Compute the range of the target using pinhole camera model.
 *
 * Uses known target width defined in TARGET_WIDTH.
 *
 * @param target_width    Width of the detected target in pixels.
 * @return  Range of the target in millimeters.
 */
int32_t computeRange(int32_t const& target_width)
{
    // TO FILL IN
}


/**
 * Get global position of the sensor node.
 *
 * Currently uses a fixed known position.
 *
 * @param own_x   Return value of own global x in mm.
 * @param own_y   Return value of own global y in mm.
 */
void getOwnGlobalPosition(int32_t& own_x, int32_t& own_y)
{
    own_x = SENSOR_G_X;
    own_y = SENSOR_G_Y;
}

/**
 * Compute target global Cartesian coordinates from relative polar coordinates.
 *
 * @param own_x   Own global x coordinate in mm.
 * @param own_y   Own global y coordinate in mm.
 * @param range   Relative range of target in mm.
 * @param azimuth Relative azimuth of target in millidegrees.
 * @param g_x     Return value of target global x in mm.
 * @param g_y     Return value of target global y in mm.
 */
void computeGlobalCoordinates(
  int32_t const& own_x,
  int32_t const& own_y,
  int32_t const& range,
  int32_t const& azimuth,
  int32_t& g_x,
  int32_t& g_y)
{
    // TO FILL IN
}

/**
 * Send target to the shooter node.
 *
 * @param g_x   Global x of the target.
 * @param g_y   Global y of the target.
 */
void sendTarget(int32_t const& g_x, int32_t const& g_y)
{
    // TO FILL IN
}
