//   ____ ___  ____  ____
//  / __// _ \/ __ \/ __/
// / _ \/ // / /_/ / _/
// \___/____/\____/_/
//
// Mini SS Workshop
// Whitespace Week 2019

// Debug flag
// #define DEBUG

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Include Libraries                                                                     *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#include <Servo.h>
#include <stdio.h>
#include <math.h>
#ifdef DEBUG
#include <SoftwareSerial.h>
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Global Position Configuration                                                         *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define LAUNCHER_G_X        0 // in mm
#define LAUNCHER_G_Y        0 // in mm

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Hardware Pin Configuration                                                            *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define FIRE_BUTTON       7
#define TILT_SERVO        9       // use a PWM pin
#define PAN_SERVO         10      // use a PWM pin
#define FIRE_SERVO        11      // use a PWM pin

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Communication Parameters                                                              *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define START_CHAR        0xF5    // start char for XBee comms

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Physical Constants for Ballistic Calculation                                          *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
#define MUZZLE_VEL        6.0f    // in m/s
#define GRAV_CONST        9.81    // in m/s^2

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Global Variables                                                                      *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Servo panServo;
Servo tiltServo;
Servo fireServo;

#ifdef DEBUG
SoftwareSerial serial(SW_SERIAL_RX, SW_SERIAL_TX);
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Forward Declarations for Subroutines                                                  *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void setServos(int32_t const& pan, int32_t const& tilt);
int32_t computeRange(int32_t const& target_width);
void getTargetGlobalPosition();
void fireProjectile();
void getOwnGlobalPosition(int32_t& own_x, int32_t& own_y);
bool computePanTilt(int32_t const& range, int32_t const& azimuth, int32_t& pan, int32_t& tilt);
void computeRelativeCoordinates(
  int32_t const& own_x,
  int32_t const& own_y,
  int32_t const& g_x,
  int32_t const& g_y,
  int32_t& range,
  int32_t& azimuth);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Arduino Setup - This runs once only                                                   *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void setup()
{
    // Attach the Servo module to the Control Pin
    panServo.attach(PAN_SERVO);
    tiltServo.attach(TILT_SERVO);
    fireServo.attach(FIRE_SERVO);

    // Zero the Servos
    panServo.write(83); //0 pos
    tiltServo.write(97); //0 pos
    fireServo.write(90); //stop

    // Initialise the Fire Button as a input to Arduino
    pinMode(FIRE_BUTTON,INPUT);

    // Initialise the XBee Wireless Communication
    Serial.begin(9600);

    #ifdef DEBUG
    serial.begin(9600);
    #endif // DEBUG
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Arduino Loop - This loops forever                                                     *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
void loop()
{
    // bool fire_status => TRUE/FALSE variable to store if fire button is activated
    bool fire_status;

    // Step 1 : Get Target Global Position
    int32_t g_x, g_y;

    #ifdef DEBUG
    if (serial.available() > 0)
    #else
    if (Serial.available() > 0)
    #endif
    {
        #ifdef DEBUG
        uint8_t temp = serial.read();
        #else
        uint8_t temp = Serial.read();
        #endif

        if (temp == START_CHAR)
        {
            uint8_t buf[8];
            int readBytes = 0;
            while (readBytes < 8)
            {
                #ifdef DEBUG
                readBytes = readBytes + serial.readBytes(buf + readBytes, 8 - readBytes);
                #else
                readBytes = readBytes + Serial.readBytes(buf + readBytes, 8 - readBytes);
                #endif // DEBUG
            }
            memcpy(&g_x, buf, 4);
            memcpy(&g_y, buf + 4, 4);
        }
    }

    // Step 2 : Get Own Global Position
    int32_t own_x, own_y;
    getOwnGlobalPosition(own_x, own_y);

    // Step 3 : Compute Relative Coordinates of Target
    int32_t range, azimuth;
    computeRelativeCoordinates(own_x, own_y, g_x, g_y, range, azimuth);

    // Step 4 : Compute Pan and Tilt necessary to Hit Target
    int32_t pan, tilt;
    bool isValid = computePanTilt(range, azimuth, pan, tilt);

    // Step 5 : Set Servos to the Computed Pan and Tilt
    #ifndef DEBUG
    if (isValid) setServos(pan, tilt);
    #endif

    // Step 6 : Fire projectile if button is pressed
    fire_status = digitalRead(FIRE_BUTTON);
    if (fire_status) fireProjectile();

    #ifdef DEBUG
    Serial.print("r:");
    Serial.print(range);
    Serial.print(" t:");
    Serial.print(tilt);
    Serial.println();
    #endif
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Function Definitions - These are the processing logics behind each line in loop()     *
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

/**
 * Set the gimbal to desired pan and tilt.
 *
 * @param pan   Pan angle in millidegrees.
 * @param tilt  Tilt angle in millidegrees.
 */
void setServos(int32_t const& pan, int32_t const& tilt)
{
    // TO FILL IN
}

/**
 * Get global position of the sensor node.
 *
 * Currently uses a fixed known position.
 *
 * @param own_x   Return value of own global x in mm.
 * @param own_y   Return value of own global y in mm.
 */
void getOwnGlobalPosition(int32_t& own_x, int32_t& own_y)
{
    own_x = LAUNCHER_G_X;
    own_y = LAUNCHER_G_Y;
}

/**
 * Compute target relative polar coordinates from global Cartesian coordinates.
 *
 * @param own_x   Own global x coordinate in mm.
 * @param own_y   Own global y coordinate in mm.
 * @param g_x     Target global x in mm.
 * @param g_y     Target global y in mm.
 * @param range   Return value of relative range of target in mm.
 * @param azimuth Return value of relative azimuth of target in millidegrees CCW.
 */
void computeRelativeCoordinates(
  int32_t const& own_x,
  int32_t const& own_y,
  int32_t const& g_x,
  int32_t const& g_y,
  int32_t& range,
  int32_t& azimuth
  )
{
    // TO FILL IN
}

/**
 * Compute the ballistic solution and corresponding pan-tilt settings.
 *
 * @param range     Target relative range in mm.
 * @param azimuth   Target relative azimuth in millidegrees.
 * @param pan       Pan angle in millidegrees.
 * @param tilt      Tilt angle in millidegrees.
 * @return True if valid solution, false otherwise.
 */
bool computePanTilt(int32_t const& range, int32_t const& azimuth, int32_t& pan, int32_t& tilt)
{
    // TO FILL IN
}

/**
 * Actuate the firing servo to release the trigger
 */
void fireProjectile()
{
    // TO FILL IN
}
